#!/bin/python

import sys
import time

import numpy as np
from numba import njit, prange

from full_aperture import green
from partial_aperture import (generate_transducters, imaging_KM, imaging_MU,
                              imaging_RT, imaging_TH, plot_heatmap)


@njit(parallel = True)
def generate_dataset(w, green, transdus, x_ref):
    '''Generate the problem's dataset
    w        : Omega (float)
    green    : Callable instance to compute the green function (in frequency domain)
    transdus : Array of the transducters's position'''
    N = transdus.shape[0]
    M = w.shape[0]
    data = np.zeros((M,N,N), dtype=np.complex128)
    for m in prange(M):
        for s in prange(N):
            for r in prange(N):
                data[m,r,s] = w[m]**2*green(w[m], transdus[s], x_ref)*green(w[m], x_ref, transdus[r])
    return data

@njit(parallel=True)
def heatmap(f, green, transdus, data, w, R0=100, x_min=-100, x_max=100, size=100):
    u = np.linspace(-50, 50, size)
    l = np.linspace(0, 100, size)
    M = w.shape[0]
    i_rt = np.zeros((size, size), dtype=np.float64)
    for i in prange(size):
        for j in prange(size):
            for m in range(M):
                i_rt[j,i] += abs(f(np.array([[l[i]],[u[j]]]), green, transdus, data[m], w[m], np.zeros((2,1)), R0))
    return i_rt

def main():
    # PARAMETERS DEFINITION
    OMEGA = np.pi
    n_w   = 20
    B     = .05*OMEGA
    w     = np.linspace(OMEGA-B, OMEGA+B, n_w)
    R0    = 100
    N     = 25
    x_ref = np.array([[60.],[0.]])
    # DATASET
    start    = time.time()
    transdus = generate_transducters(N, R0)
    data     = generate_dataset(w, green, transdus, x_ref)
    i_rt     = heatmap(imaging_KM, green, transdus, data, w)
    stop     = time.time()
    # PLOT
    print("Elapsed time: {:.2f} s".format(stop-start))
    plot_heatmap(i_rt, x_ref, R0, transdus)


if __name__=="__main__":
    main()
