# -*- coding: utf-8 -*-
#!/bin/python

import numpy as np
from numpy.random import randn
import matplotlib.pyplot as plt
from numba_port_scipy_special import j0_njit, y0_njit

from numba import jit, prange

# plt.close('all')

def wavelength(l0):
    w = 2*np.pi/l0
    print('pulsation : {:.2f}'.format(w/np.pi) + ' [pi]')
    return w

def SNR(N, std, l0, R0):
    return 20*np.log(N/(std*4*(l0**2)*(R0**2)))

##### Parameters of the model
R0 = 100.
xRef = np.array([[10.],[20.]])
l0 = 2
w = wavelength(l0) # wavelength(2) := pi

@jit(nopython=True)
def G0(w,x,y):
    ''' time harmonic Green function in the homogenous medium '''
    s = (w*np.sqrt(np.dot((x-y).T, x-y)))[0,0]
    return (1j*j0_njit(s) - y0_njit(s))/4

@jit(nopython=True)
def arrayTransducers(N,R0):
    ''' compute the position of the array of transducers : uniformly distributed over the circle C(0,R0) '''
    theta = np.linspace(0, 2*np.pi, N)
    xTransducers = np.zeros((N,2,1), dtype=np.float64)
    i = 0
    for t in theta:
        xTransducers[i] = np.array([[np.cos(t)],[np.sin(t)]])
        i += 1
    return xTransducers*R0

@jit(nopython=True)
def dataSet(w, std, xTransducers, N, xRef):
    ''' compute the synthetic dataset vector of recorded waves by the array of transducers '''
    u = np.zeros(N**2, dtype=np.complex128)
    i = 0
    for xr in xTransducers:
        for xs in xTransducers:
            u[i] = G0(w,xr,xRef)*G0(w,xRef,xs)*(w**2) + std*(randn() + 1j*randn())/np.sqrt(2)
            i += 1
    return u

@jit(nopython=True)
def adjointA(w, xTransducers, N, yS):
    A = np.zeros(N**2, dtype=np.complex128)
    i = 0
    for xr in xTransducers:
        for xs in xTransducers:
            A[i] = np.conj(G0(w,yS,xr)*G0(w,xs,yS))
            i += 1
    return A

@jit(nopython=True)
def I_RT(u, w, xTransducers, N, yS):
    A = adjointA(w, xTransducers, N, yS)
    return np.dot(A,u)

@jit(nopython=True)
def gridSearch(R0, nPoints=100):
    X,Y = np.linspace(-R0, R0, nPoints), np.linspace(-R0, R0, nPoints)
    yS = np.zeros((nPoints**2,2,1), dtype=np.float64)
    i = 0
    for x in X:
        for y in Y:
            yS[i] = np.array([[x],[y]])
            i += 1
    return yS

def gridFocalSpot(xRef, R0, zoom=10, nPoints=25):
    xR,zR = xRef[0,0], xRef[1,0]
    X = np.linspace(np.max((-R0, xR-R0/zoom)), np.min((R0, xR+R0/zoom)), nPoints)
    Y = np.linspace(np.max((-R0, zR-R0/zoom)), np.min((R0, zR+R0/zoom)), nPoints)
    yS = np.zeros((nPoints**2,2,1), dtype=np.float64)
    i = 0
    for x in X:
        for y in Y:
            yS[i] = np.array([[x],[y]])
            i += 1
    return yS

@jit(nopython=True, parallel = True)
def MUSIC_type_imaging(u, w, xTransducers, N, yS, nPoints=25):
    u = u.reshape((N,N))
    _,v = np.linalg.eigh(u)
    e1 = v[:,0] # first eigenvector of the data set matrix u
    
    imageMedium = np.zeros((nPoints,nPoints), dtype=np.float64)
    for i in prange(nPoints**2):
        g = np.zeros(N, dtype=np.complex128); k = 0
        for xR in xTransducers:
            g[k] = G0(w,xR, yS[i])
            k += 1
        imageMedium[i//nPoints,i%nPoints] = np.abs(np.dot(g,e1))**2
    return imageMedium


#### Simulation Full-Aperture RT localization on the Zoomed Grid around xRef
fZoom = 80
nT = 40; nTrials = 30
Std = np.linspace(0.001, 0.04137730666666666, 5)
# Std = np.linspace(0.033854159999999994, 0.04137730666666666, 3)
N = 100
# std = 0.0
# nTransducers = np.array([i for i in range(20, 80, 20)])

yT = gridFocalSpot(xRef, R0, zoom=fZoom, nPoints=nT)

with open('full_aperture_image_v5.npy', 'w') as f:
    
    XErr, ZErr = np.zeros(len(Std)), np.zeros(len(Std))
    XCov, ZCov = np.zeros(len(Std)), np.zeros(len(Std))
    i = 0
    for std in Std:
        xTransducers = arrayTransducers(N,R0)
        
        xTrial, zTrial = np.zeros(nTrials), np.zeros(nTrials)
        for k in range(nTrials):
            u = dataSet(w, std, xTransducers, N, xRef)
            iM = MUSIC_type_imaging(u, w, xTransducers, N, yT, nPoints=nT)
            max_iM = np.argmax(iM.T)
            
            # np.save(f, iM)
            
            xErr = np.abs((max_iM%nT)*(2*R0/(nT-1)) - R0)/fZoom;  xTrial[k] = xErr
            zErr = np.abs((max_iM//nT)*(2*R0/(nT-1)) - R0)/fZoom; zTrial[k] = zErr
            print('k :', k)
        
        xErrM, zErrM = np.mean(xTrial), np.mean(zTrial)
        XErr[i] = xErrM; ZErr[i] = zErrM
        XCov[i] = np.mean((xTrial-xErrM)**2); ZCov[i] = np.mean((zTrial-zErrM)**2)
        print('i :', i); i += 1
    
        plt.figure()
        plt.imshow(iM.T, cmap='viridis', interpolation='spline16')
        nTick = min(15, int(2*R0/fZoom)-1)
        Ticks = [i for i in range(0, nT, int(nT/nTick))]
        xm, xM = np.max((-R0, xRef[0,0]-R0/fZoom)), np.min((R0, xRef[0,0]+R0/fZoom))
        labsX  = [i for i in range(int(xm), int(xM), int((xM-xm)/(nTick*l0)))]
        ym, yM = np.max((-R0, xRef[1,0]-R0/fZoom)), np.min((R0, xRef[1,0]+R0/fZoom))
        labsY  = [i for i in range(int(ym), int(yM), int((yM-ym)/(nTick*l0)))]
        #plt.xticks(Ticks, labels=labsX, fontsize='x-small')
        #plt.yticks(Ticks, labels=labsY, fontsize='x-small')
        plt.plot((nT-1)/2 , (nT-1)/2  , 'r.')
        plt.plot(max_iM%nT , max_iM//nT  , 'r+')
        plt.colorbar()
        plt.title('MUSIC localization - SNR = {:.1f} dB - nTransducers = {}'.format(SNR(N,std,l0,R0), N), fontsize='small')
        plt.show()


