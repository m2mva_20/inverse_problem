# -*- coding: utf-8 -*-
#!/bin/python

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

def wavelength(l0):
    w = 2*np.pi/l0
    print('pulsation : {:.2f}'.format(w/np.pi) + ' [pi]')
    return w

def SNR(N, std, l0, R0):
    return 20*np.log(N/(std*4*(l0**2)*(R0**2)))

##### Parameters of the model
R0 = 100.
xRef = np.array([[10.],[20.]])
l0 = 2
w = wavelength(l0) # wavelength(2) := pi

fZoom = 80
nT = 40;
nTrials = 10; Std = np.linspace(0.033854159999999994, 0.04137730666666666, 10)
# nTrials = 30; Std = np.linspace(0.02256944*1, 0.02256944*2.5, 10)
N = 100

ErrDiscretizationCorection = 0.12760275152175865 ## for fZoom = 80 //  # 0.186557510803974 ## for fZoom = 15

with open('full_aperture_image_v5.npy', 'rb') as f:

    XErr, ZErr = np.zeros(len(Std)), np.zeros(len(Std)); Err_Avg = np.zeros(len(Std))
    XCov, ZCov = np.zeros(len(Std)), np.zeros(len(Std)); Proba = np.zeros(len(Std))
    i = 0    
    for std in Std:
        
        imageMedium = np.zeros((nT,nT), dtype=np.float64)
        xTrial, zTrial = np.zeros(nTrials), np.zeros(nTrials)
        Acc = 0.0
        for k in range(nTrials):
            iM = np.load('full_aperture_image_v5.npy', allow_pickle=True)
            max_iM = np.argmax(iM.T)
            imageMedium = iM

            xErr = np.abs((max_iM%nT)*(2*R0/(nT-1)) - R0)/fZoom;  xTrial[k] = xErr
            zErr = np.abs((max_iM//nT)*(2*R0/(nT-1)) - R0)/fZoom; zTrial[k] = zErr
            Err = np.sqrt(xErr**2 + zErr**2)/l0
            if Err < .5:
                Acc += 1

            # if i==2:
            #     max_imageMedium = np.argmax(imageMedium.T)
            #     plt.figure()
            #     plt.imshow(imageMedium.T, cmap='viridis', interpolation='spline16')
            #     nTick = min(15, int(2*R0/fZoom)-1)
            #     Ticks = [i for i in range(0, nT, int(nT/nTick))]
            #     xm, xM = np.max((-R0, xRef[0,0]-R0/fZoom)), np.min((R0, xRef[0,0]+R0/fZoom))
            #     labsX  = [i for i in range(int(xm), int(xM), int((xM-xm)/nTick))]
            #     ym, yM = np.max((-R0, xRef[1,0]-R0/fZoom)), np.min((R0, xRef[1,0]+R0/fZoom))
            #     labsY  = [i for i in range(int(ym), int(yM), int((yM-ym)/nTick))]
            #     plt.xticks(Ticks, labels=labsX, fontsize='x-small')
            #     plt.yticks(Ticks, labels=labsY, fontsize='x-small')
            #     plt.plot((nT-1)/2 , (nT-1)/2  , 'r.')
            #     plt.plot(max_imageMedium%nT , max_imageMedium//nT  , 'r+')
            #     plt.colorbar()
            #     plt.title('RT localization Avg - SNR = {:.1f} dB - nTransducers = {}'.format(SNR(N,std,l0,R0), N), fontsize='small')
            #     plt.show()

        xErrM, zErrM = np.mean(xTrial), np.mean(zTrial)
        XErr[i] = xErrM; ZErr[i] = zErrM
        XCov[i] = np.mean((xTrial-xErrM)**2); ZCov[i] = np.mean((zTrial-zErrM)**2)
        Proba[i] = Acc/nTrials
        
        max_imageMedium = np.argmax(imageMedium.T)
        # max_imageMedium_Avg = np.argmax((imageMedium/nTrials).T)
        
        xErr = np.abs((max_imageMedium%nT)*(2*R0/(nT-1)) - R0)/fZoom
        zErr = np.abs((max_imageMedium//nT)*(2*R0/(nT-1)) - R0)/fZoom
        Err_Avg[i] = np.sqrt(xErr**2 + zErr*2)/l0
        i += 1
        
        plt.figure()
        plt.imshow(imageMedium.T, cmap='viridis', interpolation='spline16')
        nTick = min(15, int(2*R0/fZoom)-1)
        Ticks = [i for i in range(0, nT, int(nT/nTick))]
        xm, xM = np.max((-R0, xRef[0,0]-R0/fZoom)), np.min((R0, xRef[0,0]+R0/fZoom))
        labsX  = [i for i in range(int(xm), int(xM), int((xM-xm)/nTick))]
        ym, yM = np.max((-R0, xRef[1,0]-R0/fZoom)), np.min((R0, xRef[1,0]+R0/fZoom))
        labsY  = [i for i in range(int(ym), int(yM), int((yM-ym)/nTick))]
        plt.xticks(Ticks, labels=labsX, fontsize='x-small')
        plt.yticks(Ticks, labels=labsY, fontsize='x-small')
        plt.plot((nT-1)/2 , (nT-1)/2  , 'r.')
        plt.plot(max_imageMedium%nT , max_imageMedium//nT  , 'r+')
        plt.colorbar()
        plt.title('MUSIC imaging - SNR = {:.1f} dB - nTransducers = {}'.format(SNR(N,std,l0,R0), N), fontsize='small')
        plt.show()
             
    Err = np.sqrt(XErr**2 + ZErr*2)/l0; Cov = np.sqrt(XCov**2 + ZCov**2)/l0**2 # /l0 pour exprimer en unité de [longueur d'onde]
    
    plt.figure()
    d = 'seagreen'
    plt.errorbar(Std, Err-ErrDiscretizationCorection, yerr=np.sqrt(Cov), marker='.', linestyle='--',  markerfacecolor='r', markeredgecolor='r', label='naive strategy')
    # plt.errorbar(Std*1.01, Err_Avg-ErrDiscretizationCorection, yerr=np.sqrt(Cov/nTrials), marker='.', linestyle='--',  markerfacecolor=d, markeredgecolor=d, label='empirical mean - {} trials'.format(nTrials))
    plt.title(r'Influence of the noise level $\sigma_{mes}$ on the localization error')
    plt.ylabel(r'localization error $[\lambda]$')
    plt.xlabel(r'SNR($\sigma_{mes}$) for $N=100$, $\lambda_0=2$, $R_0=100$')
    plt.xticks(Std, np.round(SNR(N,Std,l0,R0), decimals=1))
    plt.legend(loc='upper left')
    plt.show()
    
    plt.figure()
    plt.plot(Std, Proba, '--.')
    plt.title(r'Influence of the noise level $\sigma_{mes}$ on the localization error' + ' - nTrials = {}'.format(nTrials))
    plt.ylabel(r'$\mathbb{P}(Err<\frac{\lambda}{2})$ ie. being localized in the focal spot')
    plt.xlabel(r'SNR($\sigma_{mes}$) for $N=100$, $\lambda_0=2$, $R_0=100$')
    plt.xticks(Std, np.round(SNR(N,Std,l0,R0), decimals=1))
    # plt.legend(loc='upper right')
    plt.show()    
