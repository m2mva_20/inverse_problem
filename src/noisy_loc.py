#!/bin/python

import sys
import time

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
from numba import njit, prange
from tqdm import tqdm

from full_aperture import (generate_transducters, green, heatmap, imaging_KM,
                           imaging_RT, norm, plot_heatmap)
from partial_aperture import imaging_MU


@njit
def generate_dataset(w, green, transdus, x_ref, sigma):
    '''Generate the problem's dataset

    PARAMETERS
    ----------
    w        : Omega (float)
    green    : Callable instance to compute the green function (in frequency domain)
    transdus : Array of the transducters's position
    x_ref    : Position of the reflector
    ----------
    RETURN   : 2D dimension array (NxN) of complex values
    '''
    N = transdus.shape[0]
    data = np.zeros((N,N), dtype=np.complex128)
    for s in range(N):
        for r in range(N):
            data[r,s] = w**2*green(w, transdus[s], x_ref)*green(w, x_ref, transdus[r]) + sigma*rd.randn() + sigma*1j*rd.randn()
    return data

@njit
def snr(n, sigma, lamb, L):
    if sigma==0:
        return 0
    return n/(4*sigma*lamb**2*L**2)

def plot_heatmap(i_rt, xRef, R0, transdus, w, x_min, x_max, size):
    '''Plot the heatmap generated by an imaging function

    PARAMETERS
    ----------
    i_rt     : Array of the imaging values
    xRef     : Position of the reflector
    R0       : Radius of the dataset
    transdus : Position of the transducers (List of N arrays (dimension 2x1))
    ---------
    RETURN : None
    '''
    plt.figure()
    plt.imshow(i_rt)
    p_est = np.unravel_index(np.argmax(i_rt), i_rt.shape)
    p_ref = x_to_p(xRef, x_min, x_max, size)
    plt.plot(p_est[0], p_est[1], 'g.')
    plt.plot(p_ref[0], p_ref[1], 'r.')
    plt.title('Localization error = {:.2f}'.format(compute_loss_error(p_to_x(p_est, x_min, x_max, size), xRef)))
    plt.show()

def compute_loss_error(x_est, x_ref):
    '''Compute the localization error

    PARAMETERS
    ----------
    x_est : Estimated position of the reflector ((2x1) array)
    x_ref : Real position of the reflector ((2x1) array)
    ----------
    RETURN : Value of the localization error (float64)
    '''
    return norm(x_est-x_ref)

def p_to_x(p, x_min, x_max, size):
    '''Compute the coordinates conversion from real-world space to picture space

    PARAMETERS
    ----------
    p     : Coordinates to convert ((2,) dimension array)
    x_min : Lower bound of the grid ((2x1) dimension array)
    x_max : Upper bound of the grid ((2x1) dimension array)
    size  : Size of the grid
    ----------
    RETURN : converted coordinates ((2x1) dimension array)
    '''
    x = np.zeros((2,1))
    x[0,0] = (x_max-x_min)/size*p[0] + x_min
    x[1,0] = (x_min-x_max)/size*p[1]+x_max
    return x

def x_to_p(x, x_min, x_max, size):
    '''Compute the coordinates conversion from real-world space to picture space

    PARAMETERS
    ----------
    x     : Coordinates to convert ((2x1) dimension array)
    x_min : Lower bound of the grid ((2x1) dimension array)
    x_max : Upper bound of the grid ((2x1) dimension array)
    size  : Size of the grid
    ----------
    RETURN : converted coordinates ((2,) dimension array)
    '''
    p = np.zeros(2)
    p[0] = size/(x_max-x_min)*x[0,0] + size*x_min/(x_min-x_max)
    p[1] = size/(x_min-x_max)*x[1,0] + size*x_max/(x_max-x_min)
    return p

def main():
    # PARAMETERS DEFINITION
    OMEGA = np.pi
    R0    = 75
    N     = 100
    sigma = 8e-3
    x_ref = np.array([[0.],[0.]])
    x_min = -3
    x_max = 3
    size  = 100
    #  DATASET
    r        = R0
    start    = time.time()
    transdus = generate_transducters(N, r)
    data     = generate_dataset(OMEGA, green, transdus, x_ref, sigma)
    i_rt     = heatmap(imaging_RT, green, transdus, data, OMEGA, x_ref, r, x_min=x_min, x_max=x_max, size=size)
    stop     = time.time()
    # PLOT
    print("Elapsed time: {:.2f} s".format(stop-start))
    # plot_heatmap(i_rt, x_ref, R0, transdus, OMEGA)
    plot_heatmap(i_rt, x_ref, R0, transdus, OMEGA, x_min, x_max, size)

if __name__=='__main__':
    main()
