#! /bin/python

import ctypes
import os
import time

import matplotlib.pyplot as plt
import numpy as np
from numba import jit, njit, prange, vectorize
from numba.extending import get_cython_function_address
from scipy import integrate
from scipy.special import hankel1
from tqdm import tqdm

'''*************************************
The following section implements the numba's vectorization of some useful
routines from scipy.special
****************************************'''

addr = get_cython_function_address("scipy.special.cython_special", "y0")
functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double)
y0_fn = functype(addr)


@vectorize('float64(float64)')
def vec_y0(x):
    return y0_fn(x)

@njit
def y0_njit(x):
    return vec_y0(x)

addr = get_cython_function_address("scipy.special.cython_special", "j0")
functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double)
j0_fn = functype(addr)

@vectorize('float64(float64)')
def vec_j0(x):
    return j0_fn(x)

@njit
def j0_njit(x):
    return vec_j0(x)

'''*************************************
                END
****************************************'''

@njit
def norm(x):
    return np.sqrt(x.T@x)[0,0]

@njit
def green(w, x, y):
    s = w*norm(x-y)
    g =.25*(1j*j0_njit(s) - y0_njit(s))
    return g
@njit
def generate_transducters(N, R0, C = np.array([[0],[0]])):
    '''Generate the position array of all the transducters in a circular array

    PARAMETERS
    ----------
    N      : Number of transducters (int)
    R0     : Radius of the array (float)
    C      : Center of the array (default (0,0))
    ----------
    RETURN : List of the N transducters coordinates
    '''
    angles = np.linspace(0,2*np.pi, N)
    coord  = np.zeros((N,2,1))
    for i in range(N):
        a = angles[i]
        coord[i,:,:] = np.array([[R0*np.cos(a) + C[0,0]], [R0*np.sin(a)+C[1,0]]])
    return coord

@njit
def generate_dataset(w, green, transdus, x_ref):
    '''Generate the problem's dataset

    PARAMETERS
    ----------
    w        : Omega (float)
    green    : Callable instance to compute the green function (in frequency domain)
    transdus : Array of the transducters's position
    x_ref    : Position of the reflector
    ----------
    RETURN   : 2D dimension array (NxN) of complex values
    '''
    N = transdus.shape[0]
    data = np.zeros((N,N), dtype=np.complex128)
    for s in range(N):
        for r in range(N):
            data[r,s] = w**2*green(w, transdus[s], x_ref)*green(w, x_ref, transdus[r])
    return data

@njit
def imaging_RT(ys, green, transdus, data, w, x_ref, R0):
    '''Compute the reverse time imaging function

    PARAMETERS
    ----------
    ys       : Evaluation point of the RT imaging function (2x1 dimension array)
    green    : Callable instance computing the green function
    transdus : Position of the transducers (List of N (2x1) dimension array)
    data     : Problem's dataset ((NxN) array of complex values)
    w        : Frequency (float64)
    x_ref    : Position of the reflector (only here to standardize the imaging function prototype)
    R0       : Radius of the dataset (only here to standardize the imaging function prototype)
    ----------
    RETURN : Value of the RT imaging function at point ys (complex64)
    '''
    N    = transdus.shape[0]
    buff = np.zeros((N,N), dtype = np.complex128)
    for r in range(N):
        for s in range(N):
            buff[r,s] = green(w, ys, transdus[r])*green(w, transdus[s], ys)*(data[r,s]).conjugate()
    return np.sum(buff)

@njit
def imaging_KM(ys, green, transdus, data, w, x_ref, R0):
    '''Compute the Kirchoff migration imaging function

    PARAMETERS
    ----------
    ys       : Evaluation point of the KM imaging function (2x1 dimension array)
    green    : Callable function computing the green function
    transdus : Position of the transducers (List of N (2x1) dimension array)
    data     : Problem's dataset ((NxN) array of complex values)
    w        : Frequency (float64)
    x_ref    : Position of the reflector (only here to standardize the imaging function prototype)
    R0       : Radius of the dataset (only here to standardize the imaging function prototype)
    ----------
    RETURN : Value of the KM imaging function at point ys (complex64)
    '''
    N    = transdus.shape[0]
    buff = np.zeros((N,N), dtype=np.complex128)
    for r in range(N):
        for s in range(N):
            tau_1 = np.angle(green(w, ys, transdus[r]))
            tau_2 = np.angle(green(w, transdus[s], ys))
            if tau_1 < 0:
                tau_1 += 2*np.pi
            if tau_2 < 0:
                tau_2 += 2*np.pi
            buff[r,s] = np.exp(-1j*(tau_1+tau_2))*data[r,s]
    return np.sum(buff)


@njit(parallel=True)
def heatmap(f, green, transdus, data, w ,x_ref , R0, x_min=-100, x_max=100, size=100):
    '''Compute all the imaging values over a given grid

    PARAMETERS
    ----------
    f        : Callable function computing the imaging value at a given point
    green    : Callable function computing the green function
    transdus : Position of the transducers (List of N (2x1) dimension array)
    data     : Problem's dataset ((NxN) array of complex values)
    w        : Frequency (float64)
    x_ref    : Position of the reflector ((2x1) array)
    R0       : Radius of the dataset
    x_min    : Lower bound of the grid
    x_max    : Upper bound of the grid
    size     : Dimension of the grid
    ----------
    RETURN : (size x size) Array of the imaging value (float64)
    '''
    u = np.linspace(x_min, x_max, size)
    i_rt = np.zeros((size, size), dtype=np.float64)
    for i in prange(size):
        for j in prange(size):
            if u[i]**2 + u[j]**2 < (R0*0.99)**2:
                i_rt[j,i] = abs(f(np.array([[u[i]],[u[j]]]), green, transdus, data, w, x_ref, R0))
    return i_rt

def plot_heatmap(i_rt, xRef, R0, transdus):
    '''Plot the heatmap generated by an imaging function

    PARAMETERS
    ----------
    i_rt     : Array of the imaging values
    xRef     : Position of the reflector
    R0       : Radius of the dataset
    transdus : Position of the transducers (List of N arrays (dimension 2x1))
    ---------
    RETURN : None
    '''
    step = 4
    nP = 100
    plt.figure()
    plt.imshow(i_rt, cmap='viridis', interpolation='spline16')
    Ticks = [i for i in range(0, nP, step)]
    labs  = [i for i in range(-int(R0), int(R0), int(2*step*R0/nP))]
    plt.xticks(Ticks, labels=labs, fontsize='x-small')
    plt.yticks(Ticks, labels=labs, fontsize='x-small')
    for xT in transdus:
        plt.plot((nP-1)*(xT[0]/R0 + 1)/2 , (nP-1)*(xT[1]/R0 + 1)/2  , 'r.')
    plt.plot((nP-1)*(xRef[0]/R0 + 1)/2 , nP*(xRef[1]/R0 + 1)/2  , 'r.')
    plt.title('Time-harmonic localization - full aperture - nPoints = {}x{}'.format(nP,nP), fontsize='small')
    plt.show()


def main():
    # PARAMETERS DEFINITION
    OMEGA = np.pi
    R0    = 100
    N     = 100
    x_ref = np.array([[10],[20]])
    # DATASET
    start    = time.time()
    transdus = generate_transducters(N, R0)
    data     = generate_dataset(OMEGA, green, transdus, x_ref)
    i_rt     = heatmap(imaging_KM, green, transdus, data, OMEGA, x_ref, R0)
    stop     = time.time()
    # PLOT
    print("Elapsed time: {:.2f} s".format(stop-start))
    plot_heatmap(i_rt, x_ref, R0, transdus)



if __name__=='__main__':
    main()
