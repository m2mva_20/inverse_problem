# Reverse time and and Kirchoff migration

This repository host a work aiming to study the localization by reverse time migration. We propose an optimized python implementation of the reverse time  and Kirchoff migration. This implementation has been used to study and compare with other techniques the precision and the stability of these methods. Our results and conclusion are presented in our report accessible [here](./report/main.pdf). 

## Installation

To reproduce the results presented in the report, you will need to download all the source code and to install the `numba` library. An example notebook is provided in order to get familiarized with the use of our functions:

    $ git clone https://gitlab.com/m2mva_20/inverse_problem.git
    $ cd ./inverse_problem
    $ pip install -r requirements.txt
    $ jupyter notebook example.ipynb

## Credits

This work has been realised by Nicolas ATIENZA and Thomas CHAPALAIN during the _Inverse Problems and Imaging_ class of the MVA Master.
